import React from 'react'
import MeteringItemsList from './MeteringItemsList'
import { map, toArray, filter } from 'lodash'

export default class MeteringCategoriesList extends React.Component {

  renderItemsList(category) {
    const { defaultDisplayItem, meteringItems, loadMeteringLevels, loadMeteringScopes } = this.props

    const items = filter(meteringItems, (item) => {
      return item.category == category.id
    })

    return(
      <MeteringItemsList key={ category.id }
                         name={ category.name }
                         items={ items }
                         defaultDisplayItem={ defaultDisplayItem }
                         loadMeteringScopes= { loadMeteringScopes }
                         loadMeteringLevels={ loadMeteringLevels }/>
    )
  }

  render() {
    const { meteringCategories } = this.props
    return(
      <div className="metering-list">
        { map(toArray(meteringCategories), this.renderItemsList.bind(this)) }
      </div>
    )
  }
}
