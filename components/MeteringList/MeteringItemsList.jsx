import React, { Component } from 'react'
import MeteringItem from './MeteringItem'

export default class MeteringItemsList extends Component {
  constructor(props) {
    super(props)
    this.renderItem.bind(this)
  }

  renderItem(item) {
    return (
      <MeteringItem key={item.id} item={ item }
                    defaultDisplayItem={ this.props.defaultDisplayItem }
                    loadMeteringScopes={ this.props.loadMeteringScopes }
                    loadMeteringLevels={ this.props.loadMeteringLevels } />

      )
  }

  render() {
    return (
      <div className="metering-item">
        <h3>{ this.props.name }</h3>
        <ol>
          { this.props.items.map(item => this.renderItem(item)) }
        </ol>
      </div>
    )
  }
}
