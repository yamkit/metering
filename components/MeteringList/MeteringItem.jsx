import React, { Component } from 'react'

export default class MeteringItem extends Component {
  constructor(props) {
    super(props)
  }

  handleClick(id, e) {
    this.props.loadMeteringLevels(id)
    this.props.loadMeteringScopes(id)
    e.preventDefault()
  }

  render() {
    const { item, defaultDisplayItem } = this.props

    if(defaultDisplayItem == item.id) {
      return (
        <li onClick={ this.handleClick.bind(this, item.id) }>
          <a key={ item.id } className="active">{ item.name }</a>
        </li>
      )
    } else {
      return (
        <li onClick={ this.handleClick.bind(this, item.id) }>
          <a key={ item.id } >{ item.name }</a>
        </li>
      )
    }
  }
}
