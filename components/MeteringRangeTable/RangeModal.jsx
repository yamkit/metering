import React, { Component } from 'react'
import { Button, Form, Input, Modal, Col, Row } from 'antd'
import { merge } from 'lodash'

const createForm = Form.create
const FormItem = Form.Item
const InputGroup = Input.Group

class RangeModal extends Component {

  getValidateStatus(field) {
    const { isFieldValidating, getFieldError, getFieldValue } = this.props.form

    if (isFieldValidating(field)) {
      return 'validating'
    } else if (!!getFieldError(field)) {
      return 'error'
    } else if (getFieldValue(field)) {
      return 'success'
    }
  }

  constructor(props) {
    super(props)

    this.showModal = this.showModal.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.hideModal = this.hideModal.bind(this)

    this.state = {
      visible: false,
      width: 800
    }

    if(props.model == 'edit') {
      this.props.form.setFieldsInitialValue(this.props.currentScope)
    }
  }

  handleSubmit() {
    if(this.props.model == 'edit') {
      this.props.putMeteringScope(merge(this.props.form.getFieldsValue()))
    }

    if(this.props.model == 'add') {
      this.props.postMeteringScope(merge(this.props.form.getFieldsValue(),
                                         { instrument_kind_id: this.props.defaultDisplayItem }))
    }

    this.hideModal()
  }

  showModal() {
    this.setState({ visible: true })
  }

  hideModal() {
    this.setState({ visible: false })
  }

  operationHint() {
    if(this.props.model == 'edit') {
      return '编辑'
    } else if(this.props.model == 'add') {
      return '新增'
    }
  }

  render() {
    const { currentItem } = this.props
    const { getFieldProps, getFieldError, isFieldValidating, getFieldValue } = this.props.form

    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 9 }
    }

    function checkMinLessThanMax (rule, value, callback) {
      if (value >= getFieldValue('max')) {
        callback('最小值不能大于最大值')
      } else {
        callback()
      }
    }

    function checkMaxMoreThanMin (rule, value, callback) {
      if (value <= getFieldValue('min')) {
        callback('最大值不能小于最小值')
      } else {
        callback()
      }
    }

    const rangeMinProps = getFieldProps('min', {
      rules: [
        { required: true, message: '最小值不能为空' },
        { validator: checkMinLessThanMax.bind(this) }
      ]
    })
    const rangeMaxProps = getFieldProps('max', {
      rules: [
        { required: true, message: '最大值不能为空' },
        { validator: checkMaxMoreThanMin.bind(this) }
      ]
    })

    const showProps = getFieldProps('show', {
      rules: [
        { required: true, message: '显示不能为空' }
      ]
    })

    const modalTitle = `${this.operationHint()}测量范围`

    let itemName = null
    if(currentItem) {
      itemName = currentItem.name
    }

    return (
      <div className="btn-wrapper">
        <Button type="primary" onClick={this.showModal}>{this.operationHint()}</Button>
        <Modal title={ modalTitle } visible={this.state.visible} onOk={this.handleSubmit} onCancel={this.hideModal} width={this.state.width}>
          <Form horizontal form={this.props.form}>
            <FormItem
                {...formItemLayout}
                label="分类：">
              <p className="ant-form-text" id="meteringTitle" name="meteringTitle">{ itemName }</p>
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="最小值："
                required
                hasFeedback
                validateStatus={this.getValidateStatus('min')}>
                  <Input {...rangeMinProps} type="number" autoComplete="off" placeholder="最小值" />
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="最大值："
                required
                hasFeedback
                validateStatus={this.getValidateStatus('max')}>
                <Input {...rangeMaxProps} type="number" autoComplete="off" placeholder="最大值" />
            </FormItem>
            <FormItem
                label="显示："
                labelCol={{ span: 3 }}
                wrapperCol={{ span: 21 }}
                required
                hasFeedback
                validateStatus={this.getValidateStatus('show')}
                >
                <Row>
                  <Col span="11">
                    <Input {...showProps} type="textarea" placeholder="HTML5效果预览" rows="6" />
                  </Col>
                  <Col span="12" offset="1">
                    <div className="innerHTML" dangerouslySetInnerHTML={{ __html: this.props.form.getFieldsValue().show }} />
                  </Col>
                </Row>
            </FormItem>
          </Form>
        </Modal>
      </div>
    )
  }
}

export default createForm()(RangeModal)
