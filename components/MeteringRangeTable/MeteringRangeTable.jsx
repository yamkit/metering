import React, { Component } from 'react'
import { Table, Modal } from 'antd'
import { toArray, filter } from 'lodash'

import '../Metering.less'
import RangeModal from './RangeModal'

const showModal = (scope, deleteMeteringScope) => {
  Modal.confirm({ title: '警告', content: '确认是否删除该范围?' , onOk: () => {
    deleteMeteringScope(scope)
  } } )
}

const createColumns = (currentItem, deleteMeteringScope, putMeteringScope) => {
  return [ {
    title: '序号',
    dataIndex: 'id'
  }, {
    title: '数据范围',
    dataIndex: 'special'
  }, {
    title: '效果',
    dataIndex: 'show',
    render(text) {
      return(
        <div dangerouslySetInnerHTML={{ __html: text }} />
      )
    }
  }, {
    title: '操作',
    key: 'operation',
    render(text, scope) {
      return (
        <span>
          <RangeModal model="edit"
                      currentScope={ scope }
                      currentItem={ currentItem }
                      putMeteringScope={ putMeteringScope }/>
          <span className="ant-divider"></span>
          <a onClick={ showModal.bind(this, scope, deleteMeteringScope) }>删除</a>
        </span>
      )
    }
  } ]
}

export default class MeteringRangeTable extends Component {
  render() {
    const { currentItem, defaultDisplayItem,
            putMeteringScope, postMeteringScope,
            meteringScopes, deleteMeteringScope } = this.props

    const showScopes = toArray(meteringScopes).map((scope) => {
      return {
        id: scope.id,
        state: scope.state,
        min: scope.min,
        max: scope.max,
        special: `${scope.min}-${scope.max}`,
        show: scope.show
      }
    })

    const usedScopes = filter(showScopes, (scope) => {
      return scope.state == 'used'
    })

    return (
      <div>
        <h3>允许的测量范围</h3>
      <Table columns={ createColumns(currentItem,
                                     deleteMeteringScope,
                                     putMeteringScope) }
      dataSource={ usedScopes } pagination={false} bordered />

      <RangeModal defaultDisplayItem={ defaultDisplayItem }
                  model="add"
                  postMeteringScope={ postMeteringScope }
                  currentItem={ currentItem }
                  putMeteringScope={ putMeteringScope }/>
      </div>
    )
  }
}
