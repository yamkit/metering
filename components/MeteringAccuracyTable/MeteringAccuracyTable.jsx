import React, { Component } from 'react'
import { Table, Modal } from 'antd'
import { filter } from 'lodash'

import '../Metering.less'
import AccuracyModal from './AccuracyModal'

const showModal = (level, deleteMeteringLevel) => {
  Modal.confirm({ title: '警告', content: '确认是否删除等级!', onOk: () => {
    deleteMeteringLevel(level)
  } } )
}

const createColumns = (currentItem, putMeteringLevel, deleteMeteringLevel) => {
  return  [ {
    title: '序号',
    dataIndex: 'id'
  }, {
    title: '等级',
    dataIndex: 'level'
  }, {
    title: '效果',
    className: 'column-show',
    dataIndex: 'show',
    render(text) {
      return (
        <div dangerouslySetInnerHTML={{ __html: text }} />
      )
    }
  }, {
    title: '操作',
    key: 'operation',
    render(text, level) {
      return (
        <span>
          <AccuracyModal model="edit"
            currentLevel={ level }
            currentItem={ currentItem }
            putMeteringLevel={ putMeteringLevel } />
          <span className="ant-divider"></span>
          <a onClick={ showModal.bind(this, level, deleteMeteringLevel) }>删除</a>
        </span>
      )
    }
  } ]
}

export default class MeteringAccuracyTable extends Component {
  render() {
    const { meteringLevels, defaultDisplayItem,
            putMeteringLevel, deleteMeteringLevel,
            postMeteringLevel, currentItem } = this.props

    const filteInvalidLevels = filter(meteringLevels, (level) => {
      return level.state == 'used'
    })

    return (
      <div>
        <h3>允许的准确度等级</h3>
        <Table columns={ createColumns(currentItem, putMeteringLevel, deleteMeteringLevel) }
               dataSource={ filteInvalidLevels } pagination={false} bordered />
        <AccuracyModal defaultDisplayItem={ defaultDisplayItem }
                       model="add"
                       currentItem={ currentItem }
                       postMeteringLevel={ postMeteringLevel }/>
      </div>
    )
  }
}
