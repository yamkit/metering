import React, { Component } from 'react'
import { Button, Form, Input, Modal, Row, Col } from 'antd'
import { merge, isEmpty } from 'lodash'

const createForm = Form.create
const FormItem = Form.Item

class AccuracyModal extends Component {

  getValidateStatus(field) {
    const { isFieldValidating, getFieldError, getFieldValue } = this.props.form

    if (isFieldValidating(field)) {
      return 'validating'
    } else if (!!getFieldError(field)) {
      return 'error'
    } else if (getFieldValue(field)) {
      return 'success'
    }
  }

  constructor(props) {
    super(props)

    this.showModal = this.showModal.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.hideModal = this.hideModal.bind(this)

    this.state = {
      visible: false,
      width: 800
    }

    if(props.model == 'edit') {
      this.props.form.setFieldsInitialValue(this.props.currentLevel)
    }
  }

  handleSubmit() {
    if(this.props.model == 'add') {
      this.props.postMeteringLevel(merge(this.props.form.getFieldsValue(),
                                          { instrument_kind_id: this.props.defaultDisplayItem }))
    }
    if(this.props.model == 'edit') {
      this.props.putMeteringLevel(merge(this.props.form.getFieldsValue(),
                                        { id: this.props.currentLevel.id }))
    }

    this.hideModal()
  }

  showModal() {
    this.setState({ visible: true })
  }

  getTtile() {
    if(isEmpty(this.props.currentItem)) {
      return null
    } else {
      return this.props.currentItem.name
    }
  }

  hideModal() {
    this.setState({ visible: false })
  }

  operationHint() {
    if(this.props.model == 'edit') {
      return '编辑'
    } else if(this.props.model == 'add') {
      return '新增'
    }
  }

  render() {
    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 9 }
    }

    const levelProps = getFieldProps('level', {
      rules: [
        { required: true, message: '等级不能为空' },
      ],
    })

    const showProps = getFieldProps('show', {
      rules: [
        { required: true, message: '显示不能为空' },
      ],
    })

    const modalTitle = `${this.operationHint()}准确度等级`

    return (
      <div className="btn-wrapper">
        <Button type="primary" onClick={this.showModal}>{this.operationHint()}</Button>

        <Modal title={modalTitle} visible={this.state.visible} onOk={this.handleSubmit} onCancel={this.hideModal} width={this.state.width}>

          <Form horizontal form={this.props.form}>

            <FormItem
              {...formItemLayout}
              label="分类：">
              <p className="ant-form-text" id="meteringTitle" name="meteringTitle">{this.getTtile()}</p>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="等级："
              required
              hasFeedback
              validateStatus={this.getValidateStatus('level')}>
              <Input {...levelProps} type="text" autoComplete="off" />
            </FormItem>
            <FormItem
              label="显示："
              labelCol={{ span: 3 }}
              wrapperCol={{ span: 21 }}
              required
              hasFeedback
              validateStatus={this.getValidateStatus('show')}>
              <Row>
                <Col span="11">
                  <Input {...showProps} type="textarea" placeholder="HTML5效果预览" rows="6" />
                </Col>
                <Col span="12" offset="1">
                  <div className="innerHTML" dangerouslySetInnerHTML={{ __html: this.props.form.getFieldsValue().show }} />
                </Col>
              </Row>
            </FormItem>

          </Form>
        </Modal>
      </div>
    )
  }
}

export default createForm()(AccuracyModal)
