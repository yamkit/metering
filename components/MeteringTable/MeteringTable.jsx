import React, { Component } from 'react'
import { toArray } from 'lodash'

import MeteringAccuracyTable from '../MeteringAccuracyTable/MeteringAccuracyTable'
import MeteringRangeTable from '../MeteringRangeTable/MeteringRangeTable'
import '../Metering.less'

export default class MeteringTable extends Component {
  render() {
    const { defaultDisplayItem, meteringLevels,
            meteringScopes, loadMeteringScopes,
            putMeteringLevel, deleteMeteringLevel,
            putMeteringScope, postMeteringScope,
            deleteMeteringScope,
            postMeteringLevel, currentItem } = this.props
    const meteringLevelsArray = toArray(meteringLevels)

    let title = null
    if(this.props.currentItem) {
      title = this.props.currentItem.name
    } else {
      title = null
    }

    return (
      <div className="metering-datamanage">
        <h2>{title}</h2>
        <MeteringAccuracyTable defaultDisplayItem={ defaultDisplayItem }
                               currentItem={ currentItem }
                               putMeteringLevel={ putMeteringLevel }
                               deleteMeteringLevel={ deleteMeteringLevel }
                               postMeteringLevel={ postMeteringLevel }
                               meteringLevels={ meteringLevelsArray } />
        <MeteringRangeTable currentItem={ currentItem }
                            defaultDisplayItem={ defaultDisplayItem }
                            putMeteringScope={ putMeteringScope }
                            postMeteringScope={ postMeteringScope }
                            deleteMeteringScope ={ deleteMeteringScope }
                            meteringScopes={ meteringScopes }
                            loadMeteringScopes={ loadMeteringScopes }/>
      </div>
    )
  }
}
