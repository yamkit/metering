import { Schemas, CALL_API } from '../middlewares/api'

export const FETCH_METERING_ITEMS = 'FETCH_METERING_ITEMS'
export const FETCH_METERING_ITEMS_SUCCESS = 'FETCH_METERING_ITEMS_SUCCESS'
export const FETCH_METERING_ITEMS_FAILURE = 'FETCH_METERING_ITEMS_FAILURE'

export function fetchMeteringItems() {
  return {
    [CALL_API]: {
      types: [ FETCH_METERING_ITEMS, FETCH_METERING_ITEMS_SUCCESS,
               FETCH_METERING_ITEMS_FAILURE ],
      endpoint: '/kinds',
      method: 'GET',
      schema: Schemas.METERITEM_ARRAY
    }
  }
}

export function loadMeteringItems() {
  return (dispatch) => {
    return dispatch(fetchMeteringItems())
  }
}


export const FETCH_METERING_LEVELS = 'FETCH_METERING_LEVELS'
export const FETCH_METERING_LEVELS_SUCCESS = 'FETCH_METERING_LEVELS_SUCCESS'
export const FETCH_METERING_LEVELS_FAILURE = 'FETCH_METERING_LEVELS_FAILURE'

export function fetchMeteringLevels(kindId) {
  return {
    [CALL_API]: {
      types: [ FETCH_METERING_LEVELS, FETCH_METERING_LEVELS_SUCCESS,
               FETCH_METERING_LEVELS_FAILURE ],
      endpoint: `/levels?kindId=${kindId}`,
      schema: Schemas.METERLEVEL_ARRAY,
      method: 'GET',
      params: {
        kindId: kindId
      }
    }
  }
}


export function loadMeteringLevels(kindId) {
  return (dispatch) => {
    dispatch(fetchMeteringLevels(kindId))
  }
}

export const FETCH_METERING_SCOPES = 'FETCH_MERTERING_SCOPES'
export const FETCH_METERING_SCOPES_SUCCESS = 'FETCH_MERTERING_SCOPES_SUCCESS'
export const FETCH_METERING_SCOPES_FAILURE = 'FETCH_MERTERING_SCOPES_FAILURE'

export function fetchMeteringScopes(kindId) {
  return {
    [CALL_API]: {
      types: [ FETCH_METERING_SCOPES, FETCH_METERING_SCOPES_SUCCESS,
               FETCH_METERING_SCOPES_FAILURE ],
      endpoint: `/scopes?kindId=${kindId}`,
      schema: Schemas.METERSCOPE_ARRAY,
      method: 'GET',
      params: {
        kindId: kindId
      }
    }
  }
}

export function loadMeteringScopes(kindId) {
  return (dispatch) => {
    dispatch(fetchMeteringScopes(kindId))
  }
}

export const POST_METERING_LEVEL = 'POST_METERING_LEVEL'
export const POST_METERING_LEVEL_SUCCESS = 'POST_METRING_LEVEL_SUCCESS'
export const POST_METERING_LEVEL_FAILURE = 'POST_METERING_LEVEL_FAILURE'

export function fetchMeteringLevel(level) {
  return {
    [CALL_API]: {
      types: [ POST_METERING_LEVEL, POST_METERING_LEVEL_SUCCESS,
               POST_METERING_LEVEL_FAILURE ],
      endpoint: '/levels',
      schema: Schemas.METERLEVEL,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      params: level
    }
  }
}

export function postMeteringLevel(level) {
  return (dispatch) => {
    dispatch(fetchMeteringLevel(level))
  }
}

export const PUT_METERING_LEVEL = 'PUT_METERING_LEVEL'
export const PUT_METERING_LEVEL_SUCCESS = 'PUT_METERING_LEVEL_SUCCESS'
export const PUT_METERING_LEVEL_FALURE = 'PUT_METERING_LEVEL_FALURE'

export function editMeteringLevel(level) {
  return {
    [CALL_API]: {
      types: [ PUT_METERING_LEVEL, PUT_METERING_LEVEL_SUCCESS,
               PUT_METERING_LEVEL_FALURE ],
      endpoint: `/levels/${level.id}`,
      schema: Schemas.METERLEVEL,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      params: level
    }
  }
}

export function putMeteringLevel(level) {
  return (dispatch) => {
    dispatch(editMeteringLevel(level))
  }
}

export const DELETE_METERING_LEVEL = 'DELETE_METERING_LEVEL'
export const DELETE_METERING_LEVEL_SUCCESS = 'DELETE_METERING_LEVEL_SUCCESS'
export const DELETE_METERING_LEVEL_FAILURE = 'DELETE_METERING_LEVEL_FAILURE'

export function deleteMeteringLevel(level) {
  return (dispatch) => {
    dispatch(destroyMeteringLevel(level))
  }
}

export function destroyMeteringLevel(level) {
  return (dispatch) => {
    dispatch({
      [CALL_API]: {
        types: [ DELETE_METERING_LEVEL, DELETE_METERING_LEVEL_SUCCESS,
                DELETE_METERING_LEVEL_FAILURE ],
        endpoint: `/levels/${level.id}`,
        schema: Schemas.METERLEVEL,
        method: 'DELETE',
        headers: {
          'Content-Type':  'application/json'
        },
        params: level
      }
    })
  }
}


export const DELETE_METERING_SCOPE = 'DELETE_METERING_SCOPE'
export const DELETE_METERING_SCOPE_SUCCESS = 'DELETE_METERING_SCOPE_SUCCESS'
export const DELETE_METERING_SCOPE_FALURE = 'DELETE_METERING_SCOPE_FALURE'

export function deleteMeteringScope(scope) {
  return (dispatch) => {
    dispatch({
      [CALL_API]: {
        types: [ DELETE_METERING_SCOPE, DELETE_METERING_SCOPE_SUCCESS,
                 DELETE_METERING_SCOPE_FALURE ],
        endpoint: `/scopes/${scope.id}`,
        schema: Schemas.METERSCOPE,
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        params: scope
      }
    })
  }
}

export const PUT_METERING_SCOPE = 'PUT_METERING_SCOPE'
export const PUT_METERING_SCOPE_SUCCESS = 'PUT_METERING_SCOPE_SUCCESS'
export const PUT_METERING_SCOPE_FALURE = 'PUT_METERING_SCOPE_FALURE'

export function putMeteringScope(scope) {
  return (dispatch) => {
    dispatch({
      [CALL_API]: {
        types: [ PUT_METERING_SCOPE, PUT_METERING_SCOPE_SUCCESS,
                 PUT_METERING_SCOPE_FALURE ],
        endpoint: `/scopes/${scope.id}`,
        schema: Schemas.METERSCOPE,
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        params: scope
      }
    })
  }
}


export const POST_METERING_SCOPE = 'POST_METERING_SCOPE'
export const POST_METERING_SCOPE_SUCEESS = 'POST_METERING_SCOPE_SUCCESS'
export const POST_METERING_SCOPE_FALURE = 'POST_MERING_SCOPE_FALURE'

export function postMeteringScope(scope) {
  return (dispatch) => {
    dispatch({
      [CALL_API]: {
        types: [ POST_METERING_SCOPE, POST_METERING_SCOPE_SUCEESS,
                 POST_METERING_SCOPE_FALURE ],
        endpoint: '/scopes',
        schema: Schemas.METERSCOPE,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        params: scope
      }
    })
  }
}
