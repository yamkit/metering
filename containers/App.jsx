import React, { Component, PropTypes } from 'react'
import { loadMeteringItems, loadMeteringLevels,
         loadMeteringScopes, postMeteringLevel,
         deleteMeteringScope, putMeteringScope,
         postMeteringScope,
         deleteMeteringLevel, putMeteringLevel  } from '../actions/index'
import { connect } from 'react-redux'
import { isEmpty, toArray, findLast } from 'lodash'
import { Row, Col } from 'antd'
import MeteringCategoriesList from '../components/MeteringList/MeteringCategoriesList'
import MeteringTable from '../components/MeteringTable/MeteringTable'

import '../components/Metering.less'
import 'antd/lib/index.css'


class App extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.loadMeteringItems()
  }

  componentWillReceiveProps(nextProps) {
    const { defaultDisplayItem } = this.props
    const { loadMeteringLevels, loadMeteringScopes, meteringItems } = nextProps

    if (!isEmpty(meteringItems)) {
      if(!defaultDisplayItem) {
        loadMeteringLevels(toArray(meteringItems)[0].id)
        loadMeteringScopes(toArray(meteringItems)[0].id)
      }
    }
  }

  render() {
    const { defaultDisplayItem, meteringItems,
            meteringCategories, meteringLevels,
            postMeteringLevel, meteringScopes,
            putMeteringLevel, deleteMeteringLevel,
            deleteMeteringScope, putMeteringScope,
            postMeteringScope,
            loadMeteringLevels, loadMeteringScopes } = this.props

    const currentItem = findLast(toArray(meteringItems), (item) => {
      return item.id == defaultDisplayItem
    })

    return (
      <div>
        <h1 className="text-center">计量仪器管理</h1>
        <Row type="flex" justify="space-around" align="top">
          <Col span="6">
            <MeteringCategoriesList defaultDisplayItem={ defaultDisplayItem }
                                    meteringCategories={ meteringCategories }
                                    meteringItems={ meteringItems }
                                    loadMeteringScopes={ loadMeteringScopes}
                                    loadMeteringLevels={ loadMeteringLevels }/>
          </Col>
          <Col span="12"><MeteringTable
                             defaultDisplayItem={ defaultDisplayItem }
                             currentItem = { currentItem }
                             meteringLevels={ meteringLevels }
                             meteringScopes={ meteringScopes }
                             postMeteringLevel={ postMeteringLevel }
                             postMeteringScope={ postMeteringScope }
                             putMeteringLevel={ putMeteringLevel }
                             putMeteringScope={ putMeteringScope }
                             deleteMeteringLevel={ deleteMeteringLevel }
                             deleteMeteringScope={ deleteMeteringScope }
                             loadMeteringScopes={ loadMeteringScopes }/>
          </Col>
        </Row>
      </div>
    )
  }
}

App.propTypes = {
  meteringItems: PropTypes.object,
  meteringCategories: PropTypes.object,
  loadMeteringLevels: PropTypes.func,
  loadMeteringScopes: PropTypes.func,
  postMeteringLevel: PropTypes.func,
  putMeteringLevel: PropTypes.func,
  deleteMeteringLevel: PropTypes.func,
  putMeteringScope: PropTypes.func,
  postMeteringScope: PropTypes.func,
  errorMessage: PropTypes.string,
  children: PropTypes.node
}

function mapStateToProps(state) {
  return {
    meteringItems: state.entities.meteringItems,
    meteringCategories: state.entities.meteringCategories,
    meteringLevels: state.entities.meteringLevels,
    meteringScopes: state.entities.meteringScopes,
    defaultDisplayItem: state.entities.defaultDisplayItem,
    errorMessage: state.errorMessage
  }
}

export default connect(mapStateToProps, {
  loadMeteringItems,
  loadMeteringLevels,
  loadMeteringScopes,
  postMeteringLevel,
  putMeteringLevel,
  deleteMeteringLevel,
  deleteMeteringScope,
  putMeteringScope,
  postMeteringScope
})(App)
