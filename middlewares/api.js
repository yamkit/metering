import { Schema, arrayOf, normalize } from 'normalizr'
import { camelizeKeys } from 'humps'
import 'isomorphic-fetch'

const API_ROOT = 'http://gugud.com:8880'

function callApi(endpoint, schema, method, headers, params) {
  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint

  let promise = null
  if(method == 'GET') {
    promise = fetch(fullUrl)
  }

  if(method == 'POST' || method == 'DELETE' || method == 'PUT') {
    promise = fetch(fullUrl, {
      method: method,
      headers: headers,
      body: JSON.stringify(params)
    })
  }
  return promise.then(
    response =>
      response.json().then(json => ({ json, response }))
  ).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json)
    }

    const camelizedJson = camelizeKeys(json)

    return Object.assign({},
                         normalize(camelizedJson, schema)
                        )
  })
}

const meteringItemSchema = new Schema('meteringItems')
const meteringCategorySchema = new Schema('meteringCategories')
const meteringLevelSchema = new Schema('meteringLevels')
const meteringScopeSchema = new Schema('meteringScopes')

meteringItemSchema.define({
  category: meteringCategorySchema
})

export const Schemas  = {
  METERITEM: meteringItemSchema,
  METERITEM_ARRAY: arrayOf(meteringItemSchema),
  METERLEVEL: meteringLevelSchema,
  METERLEVEL_ARRAY: arrayOf(meteringLevelSchema),
  METERSCOPE: meteringScopeSchema,
  METERSCOPE_ARRAY: arrayOf(meteringScopeSchema)
}

export const CALL_API = Symbol('Call API')

export default store => next => action => {
  const callAPI = action[CALL_API]
  if (typeof callAPI === 'undefined') {
    return next(action)
  }

  let { endpoint } = callAPI
  const { schema, types, method, params, headers } = callAPI

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState())
  }

  if (typeof endpoint !== 'string') {
    throw new Error('Specify a string endpoint URL.')
  }
  if (!schema) {
    throw new Error('Specify one of the exported Schemas.')
  }
  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types.')
  }
  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected action types to be strings.')
  }

  function actionWith(data) {
    const finalAction = Object.assign({}, action, data)
    delete finalAction[CALL_API]
    return finalAction
  }

  const [ requestType, successType, failureType ] = types
  next(actionWith({ type: requestType }))

  return callApi(endpoint, schema, method, headers, params).then(
    response => next(actionWith({
      response,
      type: successType,
      method: method,
      params: params,
      headers: headers
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened'
    }))
  )
}
