import * as ActionTypes from '../actions'
import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import { merge, toArray } from 'lodash'

// Updates Error message to notify about the failed fetches.
function errorMessage(state = null, action) {
  const { type, error } = action

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null
  } else if (error) {
    return action.error
  }

  return state
}

function entities(state = { meteringItems: {},
                            meteringCategories: {},
                            meteringScopes: {},
                            defaultDisplayItem: null,
                            meteringLevels: {} }, action) {

  if(action.response && action.response.entities) {
    let newState = merge({},
                       state,
                         action.response.entities)

    if(action.type == ActionTypes.FETCH_METERING_ITEMS_SUCCESS) {
      const defaultDisplayItem = toArray(newState.meteringItems)[0]

      if(defaultDisplayItem != null) {
        return merge({}, newState, { defaultDisplayItem: defaultDisplayItem.id })
      } else {
        return merge({}, newState, { defaultDisplayItem: null })
      }
    }

    if(action.type == ActionTypes.FETCH_METERING_LEVELS_SUCCESS) {
      return Object.assign({},
                           newState,
                           { defaultDisplayItem: action.params.kindId },
                           { meteringLevels: action.response.entities.meteringLevels })
    }

    if(action.type == ActionTypes.FETCH_METERING_SCOPES_SUCCESS) {
      return Object.assign({},
                           newState,
                           { defaultDisplayItem: action.params.kindId },
                           { meteringScopes: action.response.entities.meteringScopes })
    }

    return newState
  }

  return state
}

const rootReducer = combineReducers({
  entities,
  errorMessage,
  routing
})

export default rootReducer
